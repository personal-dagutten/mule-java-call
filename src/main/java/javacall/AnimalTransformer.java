package javacall;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

import model.Animal;

public class AnimalTransformer extends AbstractMessageTransformer{

    public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
    	String flowVarName = message.getProperty("flowVarName", PropertyScope.INVOCATION);
    	Animal animal=new Animal(flowVarName);
        return animal;
    }
    
    
}
