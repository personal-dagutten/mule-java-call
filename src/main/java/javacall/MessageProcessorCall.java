package javacall;

import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.execution.LocationExecutionContextProvider;
import org.mule.api.processor.MessageProcessor;

public class MessageProcessorCall implements MessageProcessor  {
	
	MessageProcessor messageProcessor;
	
	@Override
	public MuleEvent process(MuleEvent event) throws MuleException {

		System.out.println("\n-------Process Executing-------");
		
		event.getMessage().setPayload("Payload Modified from Message processor");
		
		System.out.println("\n-------Process executed-------");
		return event;
	}

	
	
}
