package javacall;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

import model.Animal;


public class OnCallClass implements Callable {

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		
		eventContext.getMessage().setProperty("surname", "Martinez", PropertyScope.SESSION);	
		
		System.out.println("Session Variable updated \n");
		System.out.println("Returning Animal Object in the payload");
	
		return new Animal();
	}
	
	
}