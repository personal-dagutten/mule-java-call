package javacall;

import org.mule.api.annotations.ContainsTransformerMethods;
import org.mule.api.annotations.Transformer;
import org.mule.api.annotations.param.Payload;

import model.Animal;

@ContainsTransformerMethods
public class AnimalTransformerAnnotation {

	@Transformer
	public Animal returnAnimal(@Payload String name){
		return new Animal(name);
	}
	
}